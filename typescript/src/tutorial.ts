// console.log('typescript tutorial');
// interface someValue {
//   name: string;
//   id: number;
// }

// let someObj:someValue = {
//   name: 'random',
//   id: 123,
// };

// console.log(someObj);


// let awesomeName:string = 'shakeAndBake'
// awesomeName =  'something'
// awesomeName = awesomeName.toUpperCase();
// console.log(awesomeName);

// awesomeName = 20;

// let amount: number = 20;
// amount = 12 - 1;
// //amount = 'pants';

// let isAwesome:boolean = true;
// isAwesome = false;
// // isAwesome = 'shakeAndBake'

// //CHALLENGE

// let greeting:string = 'Hello, Typescript!';
// greeting = greeting.toLocaleUpperCase();

// let age:number = 25;
// age = age + 5;

// let isAdult: boolean = age >= 18;
// console.log(isAdult);


// // greeting = 10;
// // age= 'thirty';
// // isAdult = 'yes';

// console.log(greeting, age, isAdult);

// //************************************
// let tax:number | string = 10
// tax = 100
// tax = 'string';

// let requetStatus: 'pending' | 'success' | 'error' = 'pending';
// requetStatus = 'success'
// requetStatus = 'error'
// // requetStatus = 'random'

// //***************************************

// let notSure: any = 4;
// notSure = 'may be a string instead'
// notSure = false;

// let random;

// //**************************************

// const books = ['1984', 'Brave New World', 'Fahrenheit 451'];

// let foundBook: string;

// for(let book of books) {
//   if (book === '1984') {
//     foundBook = book;
//     foundBook.length;
//     break;
//   }
// }
// // foundBook = 10;
// // console.log(foundBook);

// //********************************************

// let discount: number | string = 20;
// discount = '20%';
// //discount = true;

// let orderStatus: 'processing' | 'shipped' | 'delivered' = 'processing';
// // orderStatus = 'cancelled'

// //******************************************

// let prices:number[] = [100,75,42,34]
// // prices.push('hello')  > error

// let fruit: string[] = ['apple', 'orange'];

// // let randomValues:[] = ['hello']
// let emptyValues = []

// let array:(string | boolean)[] = ['apple', true, 'orange', false];

// // *****************************************

// let temperatures: number[] = [20,30,25];
// // temperatures.push('hot)

// let colors: string[] = ['red','green','blue'];
// //colors.push(true);

// //******************************************

// //OBJECTS AND FUNCTIONS

// let car:{brand:string; year:number} = {brand:'toyota', year:2020};
// car.brand = 'ford'
// // car.color = 'blue';

// let car1:{brand:string; year:number} = {brand:'toyota', year:2020};

// let book = {title:'book', cost:20}
// let pen = {title:'pen', cost:10}
// let pencil = {title:'pencil'}

// // let items:{title: string; cost:number}[] = [book, pen, pencil];

// let items: { title: string; cost?: number}[] = [book, pen, pencil];
// // items[0],title = 'new book'

// //**************************************

// let bike: {brand:string; year:number} = {brand:'yamaha', year:2010};
// //bike.year = 'old'

// let laptop: {brand:string; year:number} = {brand: 'Dell', year:2020};
// // let laptop2: {brand:string; year:number} = {brand:'Dell'}

// let product1 = {title:'shirt', price:20};
// let product2 = {title:'pants'};
// let products: {title: string; price?:number}[] = [product1, product2];

// products.push({title:'shoes'});

// //**************************************

// function sayHi(name:any){
//   console.log(`Hello there ${name.toUpperCase()}`)
// }

// //- any
// //- config
// //- type

// sayHi('john');
// sayHi(3);

// //**************************************

// function calculateDiscount(price:number) {
//   const hasDiscount = true
//   if(hasDiscount){
//     // return 'Discount Applied';
//     return price;
//   }
//   return price *0.9;
// }

// const finalPrice = calculateDiscount(200);

// //****************************************

// function addThree(number:any) {
//   let anotherNumber:number = 3
//   return number + anotherNumber;
// }
// const result = addThree(3)
// const someValue = result;

// someValue.myMethod();


// //CHALLENGE

// const names:string[] = ['john', 'jane', 'jim', 'jiil'];

// function isNameInList(name:string): boolean {
//   return names.includes(name)
// }

// let nameToCheck = 'jane'

// if(isNameInList(nameToCheck)){
//   console.log(`${nameToCheck} is in the list`);
// } else {
//   console.log(`${nameToCheck} is not in the list`);
// }

// // ***************************************

// //optional and default parameters in functions
// function calculatePrice(price: number, discount: number): number {
//   return price - (discount || 0);
// }

// let priceAfterDiscount = calculatePrice(100, 20);

// function calculateScore(initialScore:number, penalty:number): number {
//   return initialScore - penalty;
// }

// let scoreAfterPenalty = calculateScore(100, 20);
// // let scoreWithoutPenalty = calculateScore(300);

// //*************************************

// function sum(message:string,...numbers:number[]):string{
//   const doubled = numbers.map((num) => num*2);
//   console.log(doubled);

//   let total = numbers.reduce((previous, current)=>{
//     return previous + current;
//   }, 0);
//   return `${message}${total}`;
// }

// // let result = sum('The total is: ', 1,2,3,4,5)

// //*****************************************

// function logMessage(message: string) {
//   console.log(message);
// }

// logMessage('Hello, Typescript');

// //******************************************

// function processInput(input:string | number){
//   if(typeof input === 'number') {
//     console.log(input*2);
//   } else {
//     console.log(input.toLowerCase());
//   }
  
// }

// processInput(10);
// processInput('Hello');

// // ****************************************

// function createEmployee({id}:{id: number}): {id:number; isActive:boolean}{
//   return {id, isActive: id % 2 === 0};
// }

// const first = createEmployee({id:1});
// const second = createEmployee({id:2});
// console.log(first, second);

// // alternative

// function createStudent(student:{id:number, name:string}):void {
//   console.log(`Welcome to the course ${student.name.toUpperCase()}!!!!`);
// }

// const newStudent = {
//   id: 5,
//   name: 'anna',
//   email: 'anna@gmail.com'
// };

// createStudent(newStudent);
// // createStudent({id:1, name:'bob', email:'bobo@gmail.com'});

// // *****************************************

// function processData(input: string | number, config: {reverse:boolean} = {reverse:false}):string | number{
//   if(typeof input === 'number'){
//     return input * input
//   } else {
//     return config.reverse? input.toUpperCase().split('').reverse().join(''): input.toUpperCase();
//   }
// }

// console.log(processData(10));
// console.log(processData('Hello'));
// console.log(processData('Hello', {reverse: true}));
// // *****************************************

// //ALIAS AND INTERFACE

// type User = { id : number; name:  string;isActive: boolean};

// const john: {id: number; name: string; isActive: boolean} = {
//   id: 1,
//   name: 'john',
//   isActive: true,
// };
// const susan: {id: number; name: string; isActive: boolean} = {
//   id: 1,
//   name: 'susan',
//   isActive: false,
// };

// function createUser(user: { id: number; name: string; isActive: boolean }): {
//   id: number;
//   name: string;
//   isActive: boolean;
// } {
//   console.log(`Hello there ${user.name.toUpperCase()} !!!`);

//   return user;
// }

// type StringOrNumber = string | number;

// let value: StringOrNumber
// value = 'hello';
// value = 123;

// type Theme = 'light' | 'dark';

// let theme: Theme;
// theme = 'dark';
// theme = 'light';

// function setTheme(t: Theme) {
//   theme = t;
// }

// setTheme('dark');

// // **************************************

// type Employee = {id: number; name:string; department:string};
// type Manager = {id:number; name:string; employees:Employee[]};

// type Staff = Employee | Manager

// function printStaffDetails(staff:Staff):void{
//   if('employees' in staff){
//     console.log(`${staff.name} is an manager in the ${staff.employees.length} employees`);
//   } else {
//     console.log(`${staff.name} is an employee in the ${staff.department} department`)
//   }
// }

// const alice: Employee = {id:1, name:'alice', department:'Sales'};
// const steve: Employee = {id:1, name:'steve', department:'HR'};

// const bob: Manager = {id:1, name:'bob', employees: [alice, steve]}

// // ****************************************

// type Book = {id:number; name:string; price:number};
// type DiscountedBook = Book & {discount: number};

// const book1:Book = {
//   id:1,
//   name: 'how to cook a dragon',
//   price: 15
// };

// const book2: Book = {
//   id: 2,
//   name: 'the secret life of unicorns',
//   price: 18
// };

// const discountedBook: DiscountedBook = {
//   id: 3,
//   name: 'Gnomes vs. Goblins: The Ultimate Guide',
//   price: 25,
//   discount: 0.15
// }

// // *****************************************

// const propName = 'age';

// type Animal = {
//   [propName]: number;
// };

// // let tiger = {age:5};
// let tiger: Animal= { [propName]: 5};

// // ****************************************

// // interface Book {
// //   readonly isbn:number;
// //   title:string;
// //   author:string;
// //   genre:string;
// //   //method
// //   printAuthor(): void
// // printTitle(message: string): string
// // printSometing: (someValue: number)v=>vnumber;
// // }

// // const deepWork = {
// //   isbn:123,
// //   title:'deep work',
// //   author:'cal newport',
// //   genre: 'self-help',
// //   printAuthor(){
// //     console.log(this.author);
// //   },
// //    printTitle(message){
// //      return `${this.title} ${message}`;
// //    },
// // };

// // first option
// // printSomething: function (someValue) {
//   // return someValue;
// // }

// // second option
// // printSomething: (someValue) => {
// //   console.log(this);
// // }

// // // third option
// // // printSomething(spomeValue) {
// //   // return someValue;
// // //}
// // printAuthor: () => {
// //   console.log(deepWork.author);
// // }

// // deepWork.isbn = 'some value';

// // ***************************************

// interface Computer {
//   readonly id:number;
//   brand:string;
//   ram:number;
//   upgradeRam(increase: number): number;
//   storage?: number;
// }

// // const laptop: Computer = {
// //   id:1,
// //   brand: 'random brand',
// //   ram: 8,
// //   upgradeRam(amount) {
// //     this.ram += amount;
// //     return this.ram;
// //   },
// // };

// // laptop.storage = 256;
// // console.log(laptop.upgradeRam(4));

// // console.log(laptop);

// // ***************************************

// interface Person {
//   name: string;
//   getDetails(): string;
// }

// interface DogOwner {
//   dogName: string;
//   getDogDetails(): string;
// }

// interface Person {
//   age: number;
// }

// const person:Person = {
//   name:'jouhn',
//   age: 21,
//   getDetails() {
//     return `Name: ${this.name}, Age: ${this.age}`;
//   },
// };

// interface Employee extends Person {
//   employeeId: number;
// }

// const employee: Employee = {
//   name:'jane',
//   age: 24,
//   employeeId: 123,
//   getDetails(){
//     return `Name: ${this.name}, Age: ${this.age}, Employee ID: ${this.employeeId}`;
//   },
// };

// console.log(employee.getDetails());

// interface Manager extends Person, DogOwner {
//   managePeople(): void;
// }

// const manager: Manager = {
//   name: 'bob',
//   age: 35,
//   dogName: 'mukku',
//   getDetails() {
//     return `Name: ${this.name}, Age: ${this.age}`;
//   },
//   getDogDetails() {
//     return `Name: ${this.dogName}`;
//   },
//   managePeople() {
//     console.log('Managing people....');
//   }
// };

// *****************************************

// function getEmployee(): Person | DogOwner | Manager {
//   const random = Math.random()

//   if(random < 0.33) {
//     return {
//       name: 'john',
//     };
//   } else if(random < 0.66){
//     return {
//       name: 'sara',
//       dogName: 'mukku',
//     };
//   } else {
//     return {
//       name: 'bob',
//       managePeople() {
//         console.log('Managing people');
//       },
//       delegateTasks() {
//         console.log('Delegating Tasks....');
//       },
//     };
//   }
// }

// interface Person {
//   name: string;
// }

// interface DogOwner extends Person {
//   dogName: string;
// }

// interface Manager extends Person {
//   managePeople(): void;
//   delegateTasks(): void;
// }

// const employee: Person | DogOwner | Manager = getEmployee();
// // console.log(employee);

// function isManager(obj: Person | DogOwner | Manager): obj is Manager {
//   // return typeof obj === 'object'
//   return 'managePeople' in obj;
// };

// // console.log(isManager(employee));

// if(isManager(employee)){
//   employee.delegateTasks();
// }

// *****************************************

// let person: [string, number] = ['john', 25];
// let date: [number, number, number] = [12, 17, 2001];

// // date.push(34);
// // date.push(34);
// // date.push(34);
// // date.push(34);
// // date.push(34);
// console.log(date)

// function getPerson(): [string, number] {
//   return ['john', 25];
// }

// let randomPerson = getPerson();
// console.log(randomPerson[0]);
// console.log(randomPerson[1]);

// let susan: [string, number?] = ['susan'];

// *****************************************

// enum ServerResponseStatus {
//   Success = 200,
//   Error = 'Error',
// }

// Object.values(ServerResponseStatus).forEach((value) => {
//   if(typeof value === 'number'){
//     console.log(value);
//   }
// })

// console.log(ServerResponseStatus);

// interface ServerResponse{
//   result: ServerResponseStatus;
//   data: string[];
// }

// function getServerResponse(): ServerResponse{
//   return {
//     result: ServerResponseStatus.Success,
//     data: ['first item', 'second item'],
//   };
// }

// const response: ServerResponse = getServerResponse();

// *****************************************


// enum UserRole {
//   Admin, 
//   Manager,
//   Employee,
// }

// type User = {
//   id: number;
//   name: string;
//   role: UserRole;
//   contact: [string, string];
// };

// function createUser(user: User): User {
//   return user;
// }

// const user: User = createUser({
//   id: 1,
//   name: 'john doe',
// role: UserRole.Admin,
//   contact: ['john doe', '1234567890'],
// });

// console.log(user);

// ****************************************

// let someValue: any = 'this is a string';

// let strLength: number = (someValue as string).length;

// type Bird = {
//   name: string;
// };

// let birdString = '{"name": "Eagle"}';
// let dogString = '{"breed": "Poodle"}';

// let birdObject = JSON.parse(birdString);
// let dogObject = JSON.parse(dogString);

// let bird = birdObject as Bird;
// let dog = dogObject as Bird;

// console.log(bird.name);
// console.log(dog.name);


// enum Status {
//   Pending = 'pending',
//   Declined = 'declined',

// }

// type User = {
//   name: string;
//   status: Status;
// };


// const statusValue = 'pending';
// const user:User = {name:'john', status:statusValue as Status} ;

// ****************************************

// let unknownValue: unknown;

// unknownValue = 'hello world';
// unknownValue = [1,2,3];
// unknownValue = 42.33455;

// if(typeof unknownValue === 'number'){
//   unknownValue.toFixed(2);

// }

// function runSomeCode() {
//   const random = Math.random()
//   if(random<0.5) {
//     throw new Error('there was error...');
//   } else {
//     throw 'some error';
//   }
// }

// try {
//   // throw 'string'
//   // throw new Error('there was error...');
//   runSomeCode()
// } catch (error) {
//   // console.log(error.message);
//   if(error instanceof Error) {
//     console.log(error.message);
//   } else {
//     console.log(error);
//   }
// }

// ***************************************

// let someValue: never = 0;

// type Theme = 'light' | 'dark'

// function checkTheme(theme: Theme) : void {
//   if(theme === 'light') {
//     console.log('light theme');
//     return;
//   }
//   if(theme === 'dark') {
//     console.log('dark theme');
//     return;
//   }
//   theme;
// }

// enum Color {
//   red,
//   blue,
// }

// function getColorName(color:Color) {
//   switch(color){
//     case Color.red:
//       return 'Red';
//     case Color.blue:
//       return 'Blue';
//     default:
//       // at build time
//       let unexpectedColor: never = color;
//       // at runtime
//       throw new Error(`Unexpected color value : ${color}`);
//   }
// }

// console.log(getColorName(Color.red));
// console.log(getColorName(Color.blue));
// // console.log(getColorName(Color.green));

// ***************************************

// // import {something} from './action';
// const name = ''

// const susan = 'susan';

// let something

// ***************************************

// import newStudent, { sayHello, person, type Student} from "./action";
// // import {someValue} from './example.js';

// sayHello('Typescript');
// console.log(newStudent);
// console.log(person);

// const anotherStudent: Student = {
//   name: 'bob',
//   age: 23
// };

// *****************************************


// type ValueType = string | number | boolean;

// let value: ValueType;
// const random = Math.random();
// value = random < 0.33 ? 'Hello' : random < 0.66 ? 123.456 : true;

// function checkValue(value:ValueType): void {
//   if(typeof value === 'string'){
//     console.log(value.toLowerCase());
//     return;
    
//   }
//   if(typeof value === 'number'){
//     console.log(value.toFixed(4));
//     return;
    
//   }

//   console.log(`boolean : ${value}`);
// }

// checkValue(value);

// ****************************************

// type Dog = { type:'dog'; name:string; bark: () => void};
// type Cat = { type:'cat'; name:string; meow: () => void};
// type Animal = Dog | Cat;

// // function makeSound(animal: Animal){
// //   if(animal.type === 'dog'){
// //     animal.bark();
// //   } else {
// //     animal.meow();
// //   }
// // }

// function makeSound(animal:Animal){
//   if('bark' in animal){
//     animal.bark();
//   } else {
//     animal.meow();
//   }
// }

// ****************************************

// function printLength(str: string | null | undefined){
//   if(str) {
//     console.log(str.length);
//   } else {
//     console.log('no string provided');
//   }
// }

// printLength('hello');
// printLength('');
// printLength(null);
// printLength();
// printLength(undefined);

// ************************************

// try {
//   throw new Error('This is an error')
// } catch (error) {
//   if(error instanceof Error) {
//     console.log(`Caught an Error object: ${error.message}`);
    
//   } else {
//     console.log('unlnown error....');
//   }
// }


// function checkInput(input:Date | string): string {
//   if(input instanceof Date){
//     return input.getFullYear().toString();
//   }
//   return input;
// }

// const year = checkInput(new Date())
// const random = checkInput('2020-04-02');

// console.log(year);
// console.log(random);

// **************************************

// type Student = {
//   name: string;
//   study: () => void;
// };

// type User = {
//   name: string;
//   login: () => void
// };

// type Person = Student | User;

// const randomPerson = (): Person => {
//   return Math.random() > 0.5 ? {name: 'john', study: () => console.log('Studying')} : {name: 'marry', login: () => console.log('Logging in')};
// };

// // const person = randomPerson();
// const person: Person = {
//   name: 'anna',
//   login: () => console.log('study...'),
// }

// function isStudent(person:Person): person is Student{
//   // return 'study' in person
//   return (person as Student).study!== undefined;
// }

// if(isStudent(person)){
//   person.study();
// } else {
//   person.login();
// }

// **************************************

// type IncrementAction = {
//   type: 'increment';
//   amount: number;
//   timestamp: number;
//   user: string;
// };

// type DecrementAction = {
//   type: 'decrement';
//   amount: number;
//   timestamp: number;
//   user: string;
// };

// type Action = IncrementAction | DecrementAction;

// function reducer(state: number, action:Action) {
//   switch(action.type) {
//     case 'increment':
//       return state + action.amount;
//     case 'decrement':
//       return state - action.amount;
//     default:
//       const unexpectedAction: never = action;
//       throw new Error(`Unexpected action: ${unexpectedAction}`);
//   }
// }


// const newState = reducer(15, {
//   user: "john",
//   type: 'increment',
//   amount: 5,
//   timestamp: 123456789,

// });

// **************************************

// // let array1: string[] = ['Apple', 'Banana', 'Mango'];
// // let array2: number[] = [1,2,3];
// // let array3: boolean[] = [true, false, true];

// let array1: Array<string> = ['Apple', 'Banana', 'Mango'];

// **************************************

// function createString(arg:string) :string {
//   return arg;
// }
// function createNumber(arg:number) :number {
//   return arg;
// }

// ***************************************

// function genericFunction<T>(arg: T): T{
//   return arg;
// }

// const someStringValue = genericFunction<string>('Hello world');
// const someNumberValue = genericFunction<number>(2);

// interface GenericInterface<T>{
//   value:T;
//   getValue: () => T;

// }

// const genericString: GenericInterface<string> = {
//   value: 'HelloWorld',
//   getValue() {
//     return this.value;
//   },
// };

// async function someFunc(): Promise<string> {
//   return 'hello world';
// }

// const result = someFunc();

// **************************************


// createArray<string>(3, 'hello'); // 3 times hello
// createArray<number>(4, 100); // 4 times 100 in an array

// ***********************************

// function generateStringArray(length: number, value: string): string[] {
//   let result: string[] = []
//   result = Array(length).fill(value);
//   return result;
// }

// // console.log(generateStringArray(3, 'hellllooooo'));

// function createArray<T>(length: number, value: T): Array<T> {
//   let result: T[] = [];
//   result = Array(length).fill(value);
//   return result;
// }

// let arrayStrings = createArrays<string>(10,'vaishnavi')
// let arrayNumbers = createArray<number>(15, 200)

// console.log(arrayStrings);
// console.log(arrayNumbers);

// ************************************

// function pair<T, U>(param1:T, param2:U): [T,U] {
//   return [param1, param2];
// }

// let result = pair<number, string>(123, 'Heyyyy');

// function processValue<T extends string | number>(value: T): T {
//   console.log(value);
//   return value;
// }

// processValue('hello');

// *************************************

// type Car = {
//   brand: string;
//   model: string;
// };

// const car: Car = {
//   brand: 'ford',
//   model: 'mustang',
// };

// type Product = {
//   name: string;
//   price: number;
// };

// const product: Product = {
//   name: 'shoes',
//   price: 1.99,
// };

// type Student = {
//   name: string;
//   age: number;
// };

// const student: Student = {
//   name: 'peter',
//   age: 20,
// };

// function printName<T extends {name: string}>(input: T): void {
//   console.log(input.name);
// }

// printName(student);
// printName(product);

// ***********************************

// interface StoreData<T>{
//   data: T[];
// }

// const storeNumbers: StoreData<number> = {
//   data: [1,2,3,4],

// };

// const randomStuff: StoreData<any> = {
//   data:['random', 1],

// };

// **************************************

// const {data} = axios.get(someUrl);

// axios.get<{name: string} [] > (someUrl);

// export class Axios {
//   get<T = any, R = AxiosResponse<T>, D = any> {
//     url: string,
//     config?: AxiosRequestConfig<D>
//   }: Promise<R>;
// }

// export interface AxiosResponse<T = any, D = any> {
//   status: number;
//   statusText: string;
//   headers: RawAxiosResponseHeaders | AxiosResponseHeaders;
//   config: InternalAxisRequestConfig<D>;
//   request?: any;
// }

// *************************************


// import {z} from 'zod';

// const url = 'https://www.course-api.com/react-tours-project';

// const tourSchema = z.object({
//   id: z.string(),
//   name: z.string(),
//   info: z.string(),
//   image: z.string(),
//   price: z.string(),
// });

// type Tour = z.infer<typeof tourSchema>;

// type Tour = {
//   id: string;
//   name: string;
//   info: string;
//   image: string;
//   price: string;
//   something: boolean;
// };

// async function fetchData(url: string) {
//   try {
//     const response = await fetch(url);
//     if (!response.ok) {
//       throw new Error(`HTTP error! status: ${response.status}`);
//     }

//     const data = await response.json();
//     return data
//   } catch (error) {
//     const errMsg =
//       error instanceof Error ? error.message : 'there was an error...';
//     console.error(errMsg);
//     // throw error;
//     return [];
//   }
// }

// const tours = await fetchData(url);
// tours.map((tour: any) => {
//   console.log(tour.name);
// });

// It returns an empty array

// ***********************************

// export {Random} from './types';

// import {z} from 'zod';
// z.

// import bcryptjs from 'bcryptjs';

// ***********************************

// let something = 'something'

// class Book {
//   // public readonly title : string;
//   // public author: string;
//   private checkedOut: boolean = false;
//   constructor(readonly title: string, public author: string) {}

//   //   this.title = title;
//   //   this.author = author;
//   // }
//   // public checkOut() {
//   //   this.checkedOut = true;
//   // }
//   // public isCheckedOut(){
//   //   return this.checkedOut;
//   // }
//   // private toggleCheckedOutStatus() {
//   //   return !this.checkedOut;
//   // }

//   get info() {
//     return `${this.title} by ${this.autor}`;
//   }
//   private set checkOut(checkedOut: boolean){
//     this.checkedOut = checkedOut;
//   }
//   get checkOut() {
//     return this.checkedOut;
//   }
//   public get someInfo() {
//     this.checkOut = true 
//     return `${this.title} by ${this.author}`; 
//   }
// }

// const deepWork = new Book('Deep Work', 'Cal Newport');
// // deepWork.checkOut();
// // console.log(deepWork.title);
// // console.log(deepWork.isCheckedOut());
// console.log(deepWork);
// console.log(deepWork.checkOut);

// ******************************************************

// interface IPerson{
//   name: string;
//   age: number;
//   greet(): void;
// }

// class Person implements IPerson {
//   constructor(public name:string, public age: number) {}

//   greet(): void {
//     console.log(`Hello my name is ${this.name} and I'm ${this.age} years old`);
//   }
// }

// const hipster = new Person('shakeAndBake', 100);
// hipster.greet();

// *****************************************************


