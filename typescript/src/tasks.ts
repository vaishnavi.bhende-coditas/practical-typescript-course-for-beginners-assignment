// console.log('tasks');

// const btn = document.querySelector<HTMLButtonElement>('.test-btn')!;

// // btn?.addEventListener

// // if(btn) {

// // }

// // btn.addEventListener

// // if(btn) {
// //   btn.disabled = true;
// // }

// btn.disabled = true;

// ************************************

const taskForm = document.querySelector<HTMLFormElement>('.form');

const formInput = document.querySelector<HTMLInputElement>('.form-input');

const taskListElement = document.querySelector<HTMLUListElement>('.list');

type Task = {
  description: string;
  isCompleted: boolean;
};

const tasks:Task[] = loadTasks();

tasks.forEach(renderTask);

function loadTasks(): Task[] {
  const storedTasks = localStorage.getItem('tasks')
  return storedTasks? JSON.parse(storedTasks): []
}

taskForm?.addEventListener('submit', (event) =>  {
  event.preventDefault();
  const taskDescription = formInput?.value;
  if(taskDescription) {

    const task: Task = {
      description: taskDescription,
      isCompleted: false,
    };
    addTask(task);  

    renderTask(task);
    // console.log(taskDescription);

    formInput.value = '';
    return;
  }
  alert('Please enter a task Desription');
})

// taskForm?.addEventListener('submit', (event) => {
//   // event.preventDefault();
//   // const taskDescription = formInput?.value;
//   // if(taskDescription) {
//   //   console.log(taskDescription);

//   //   formInput.value = '';
//   //   return;
//   // }
//   // alert('Please enter a task Desription');
// });

function addTask(task:Task): void {
  tasks.push(task);
  console.log(tasks);
  
}

function renderTask(task:Task): void {
  const taskElement = document.createElement('li')
  taskElement.textContent = task.description;

  // checkBox
  const taskCheckbox = document.createElement('input')
  taskCheckbox.type = 'checkbox';
  taskCheckbox.checked = task.isCompleted;

  // toggle checkBox

  taskCheckbox.addEventListener('change', () => {
    task.isCompleted = !task.isCompleted
    updateStorage();
  });


  taskListElement?.appendChild(taskElement);
}

function updateStorage(): void {
  localStorage.setItem('tasks', JSON.stringify(tasks));
}

// ***************************************