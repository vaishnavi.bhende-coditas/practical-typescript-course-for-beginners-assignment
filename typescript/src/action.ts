// const susan = 'susan';

// let something = 'something';

// ****************************************

export function sayHello(name: string): void{
  console.log(`Hello ${name}`);
}

export  let person = 'susan';

export type Student = {
  name: string;
  age: number;
};

const newStudent = {
  name: 'Peter',
  age: 24,
};

export default newStudent;