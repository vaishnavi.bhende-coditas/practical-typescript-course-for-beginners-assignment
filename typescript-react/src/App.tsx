import Component from './starter/10-tasks';

function App() {
  return (
    <main>
      {/* <h2>React & Typescript</h2> */}

      {/* <Component name='Vaishnavi' id={123}>
        <h2>Hey!!!!!!!!!!</h2>
      </Component>

      <Component name='Vaishnavi' id={123}/> */}

      {/* <Component/> */}

      {/* <h2>React & Typescript</h2>
      <Component type='basic' name='susan'/>
      <Component type='advanced' name='anna' email='anna@gmail.com'/> */}

    </main>
  );
}

export default App;
