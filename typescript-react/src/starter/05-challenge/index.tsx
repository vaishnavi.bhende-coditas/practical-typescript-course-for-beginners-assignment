type BasicProfileCardProps = {
  type: 'basic';
  name: string;
};

type AdvancedProfileCardProps = {
  type: 'basic';
  name: string;
  email: string;
};

type ProfileCardProps = BasicProfileCardProps | AdvancedProfileCardProps

function Component(props: ProfileCardProps) {
  const {type, name} = props;

  const alertType = type === 'basic'?'success':'danger';
  const className = `alert lert-${alertType}`;

  return (
    <article className={className}>
      <h2>user: {name}</h2>
      {/* <h2>email: {props.email}</h2> */}
    </article>
  );
}
export default Component;
