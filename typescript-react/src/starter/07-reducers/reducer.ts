export type CounterState = {
  count: number;
  status: string;
}

type UpdateCountAction = {
  type: 'increment' | 'decrement' | 'reset'
}

type SetStatusAction = {
  type: 'setStatus';
  payload: 'active' | 'inactive';
}

export const initialState: CounterState = {
  count: 0,
  status: 'Pending'
};

type CounterAction = UpdateCountAction

export const counterReducer = (
  state: CounterState, 
  action: CounterAction
): CounterState => {
  switch(action.type){
    case 'increment':
      return {...state, count: state.count = 1};
      case 'decrement':
        return {...state, count: state.count - 1};
      case 'reset':
        return {...state, count:state.count - 1};
      // case 'setStatus':
      //   return (...state, )
    default:
      return state;

  }
};