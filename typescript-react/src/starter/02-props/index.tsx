import { type PropsWithChildren } from "react";

// type ComponentProps = { name: string; id: number; children?: React.ReactNode};

type ComponentProps = PropsWithChildren<{ 
  name: string; 
  id: number; 
  children?: React.ReactNode
}>;

function Component( {name, id, children}: ComponentProps) {
  return (
    <div>
      <h1>Nmae: {name}</h1>
      <h1>ID: {id}</h1>
    </div>
  );
}
export default Component;
