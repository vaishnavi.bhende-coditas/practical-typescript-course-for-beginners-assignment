import { useEffect, useState } from 'react';
import Form from './form.tsx';
import List from './list.tsx';
import { type Task } from './types';
import { store } from '../../store.ts';

// function loadTasks(): Task[] {
//   const storedTasks = localStorage.getItem('tasks');
//   return store? JSON.parse(storedTasks): [];
// }

function updateStorage(tasks: Task[]): void {
  localStorage.setItem('tasks', JSON.stringify(tasks));
}

function Component() {
  const [tasks, setTasks] =  useState<Task[]>([]);

  const toggleTask = ({ id }: { id: string }) => {
    setTasks(
      tasks.map((task) => {
        if (task.id === id) {
          return { ...task, isCompleted: !task.isCompleted };
        }
        return task;
      })
    );
  };

  useEffect(() => {
    updateStorage(tasks)
  }, [tasks]);

  return (
    <div>
      {/* <Form addTask = {addTask} /> */}
      <List tasks = {tasks} toggleTask={toggleTask} />
    </div>
  );
}
export default Component;